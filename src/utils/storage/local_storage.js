// 设置数据
export let set = (key, data) => {
  try {
    return localStorage.setItem(key, JSON.stringify(data))
  } catch (e) {
    console.error(e)
  }
}
// 取出数据
export let get = (key) => {
  try {
    return JSON.parse(localStorage.getItem(key))
  } catch (e) {
    console.error(e)
  }
}
// 删除数据
export let remove = (key) => {
  localStorage.removeItem(key)
}
