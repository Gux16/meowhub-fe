import axios from 'axios'
import {Notification} from 'element-ui'
// 导出对象声明
let http = {}
// GET方法
// url: String
// params: Object(最好是能)
http.get = (url, params, headers) => {
  preprocess()
  return postprocess(axios.get(prefix(url), { params, headers }), url)
}
// POST方法
// 传入一个url和一个json Object data
http.post = (url, data, headers) => {
  preprocess()
  return postprocess(axios.post(prefix(url), data, { headers }), url)
}
// PUT方法更新整条
// url一个json Object data
http.put = (url, uid, data, headers) => {
  preprocess()
  return postprocess(axios.put(prefix(`${url}/${uid}`), data, { headers }), url)
}
// PATCH方法更新个别属性
// url一个json Object data
http.patch = (url, uid, data, headers) => {
  preprocess()
  return postprocess(axios.patch(prefix(`${url}/${uid}`), data, { headers }), url)
}

http.delete = (url, uid, headers) => {
  preprocess()
  return postprocess(axios.delete(prefix(`${url}/${uid}`), { headers }), url)
}

let prefix = url => `${url}`

let preprocess = () => {

}

let postprocess = async (promise, url) => {
  let resp
  console.log(url)
  try {
    resp = await promise
    if (url.split('/')[ 1 ] === 'api') {
      // 特异性接口处理
      return resp.data
    } else if (resp.data.ResultCode !== 0) {
      // 一般接口处理
      console.warn(resp.config.method, resp.config.url, resp.config.params, resp.config.data, resp.data)
      throw resp.data
    } else {
      return resp.data.Record
    }
  } catch (e) {
    handleError(e)
  }
}

let handleError = e => {
  let error = e
  if (e.ResultCode !== undefined) {
    error = e.Message
    if (error) {
      Notification.error({
        message: error,
        duration: 1000
      })
    }
  } else {
    Notification.error({
      message: error[ 'message' ],
      duration: 1000
    })
  }
}

export default http
