import NodeRSA from 'node-rsa'
import unset from 'lodash/unset'
import cloneDeep from 'lodash/cloneDeep'

export let encryptForm = (form, pubkeyData, pubkeyEncryptType, fields, newField) => {
    let pubkey = new NodeRSA();
    // let newForm = cloneDeep(form)
    let newForm = form
    pubkey.importKey(pubkeyData, pubkeyEncryptType);
    let encryptData = {}
    for (let field of fields) {
        encryptData[ field ] = newForm[ field ]
        unset(newForm, field)
    }
    let reqData = pubkey.encrypt(JSON.stringify(encryptData), 'base64');
    newForm[ newField ] = reqData
    return newForm
}