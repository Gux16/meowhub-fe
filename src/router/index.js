import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../components/Dashboard'
import ExpressLabel from '../components/ExpressLabel'
import TaskCenter from '../components/TaskCenter'
import Unimplemented from '../components/Unimplemented'
import Test from '../components/Test'
import Login from '../components/Login'

import store from '../store'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/dashboard', component: Dashboard },
    { path: '/express_label', component: ExpressLabel },
    { path: '/task_center', component: TaskCenter },
    { path: '/data_sets', component: Unimplemented },
    { path: '/test', component: Test },
    { path: '/login', component: Login },
    { path: '/', component: Test },
    { path: '*', component: Unimplemented }
  ]
})

router.beforeEach(async (to, from, next) => {
  if (store.state.user.name !== null) {
    console.log('store.state.user.name !== null', store.state.user.name)
    next()
  } else {
    if (localStorage.getItem('name')) {
      let user = await store.dispatch('user', localStorage.getItem('name'))
      console.log('user', user)
      if (user[ 0 ].name) next()
      else next('/login')
    } else {
      console.log('hehe')
      if (to.path === '/login') next()
      else next('/login')
    }
  }
})

export default router
