import Vuex from 'vuex'
import Vue from "vue";
import api from './utils/api'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    user: {
      id: null,
      name: null,
      start: null,
      current: null,
      limit: null
    }
  },
  mutations: {
    postUsername (state, { name, id }) {
      state.user.name = name
      state.user.id = id
    },
    deleteUser (state) {
      state.user = {
        id: null,
        name: null,
        start: null,
        current: null,
        limit: null
      }
      localStorage.removeItem('name')
    },
    page (state, { start, current, limit }) {
      state.user.start = start
      state.user.current = current
      state.user.limit = limit
    }
  },
  actions: {
    async user (context, name) {
      let user = await api.get(`/api/users?name=${name}`)
      if (user.length !== 0) {
        let { id, name, start, current, limit } = user[ 0 ]
        context.commit('postUsername', { name, id })
        context.commit('page', { start, current, limit })
        localStorage.setItem('name', name)
      } else {
        context.commit('deleteUser')
      }
      return user
    },
    async updatePage (context, { start, current, limit }) {
      await api.patch('/api/users', context.state.user.id, { start, current, limit })
      context.commit('page', { start, current, limit })
    }
  }

})

export default store
