import Vue from 'vue'
import App from './App.vue'

import './assets/index.scss'
// 引入jquery方便DOM操作
import './assets/jquery-vendor'
// 引入jquery-ui的draggable插件简便拖动操作
require('jquery-ui/ui/widgets/draggable')
// 利用$.ui.mouse注册自定义jquery-ui鼠标事件插件
require('./assets/custommouse')

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'

Vue.use(ElementUI)

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
})
